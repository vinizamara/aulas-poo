//Lista de números
/*
Escreva um programa TypeScript que leia uma lista
de números inteiros do usuário, e imprima o maior 
número inserido.
*/

namespace exercicio_2
{
    let lista: number[];
    let maior: number;
    lista = [1, 2, 3, 7, 4, 5];
    maior = Math.max.apply(null, lista);

    console.log(`O maior número é ${maior}`);
}
