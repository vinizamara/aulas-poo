//Números primos
/*
Escreva um programa TypeScript que imprima todos os
números primos de 1 a 50 usando a função while.
*/

namespace exercicio_1
{
    let numero: number = 1;

    while (numero <= 53)
    {
        if (numero == 2 || numero == 3 || numero == 5 || numero == 7)
        {
            console.log(`${numero}`);
        }

        if (numero != 1 && numero % 2 != 0 &&  numero % 3 != 0 && numero % 5 != 0 && numero % 7 != 0)
        {
            console.log(`${numero}`);
        }
        numero++;
    }
}
