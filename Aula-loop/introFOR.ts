namespace introFOR
{
    for(let i = 0; i < 3; i++)
    {
        console.log("Bloco 👉" + i);
    }

    //Mostrar todos os números ímpares
    for(let i = 0; i <= 100; i++)
    {
        if (i % 2 != 0)
        {
            console.log(`O número ${i} é ímpar😯`)
        }
    }
}
