//Números divisivéis por 3
/*
Escreva um programa TypeScript que leia um número 
inteiro do usuário, e imprima todos os números de
1 a esse número que são divisíveis por 3.
*/

namespace exercicio_3
{
    let numero, count: number;
    count = 1;
    numero = 15;

    while (count <= numero)
    {
        if (count % 3 == 0)
        {
            console.log(`${count}`);
        }
        count++;
    }
}
