//Nivel de conhecimento
/*
Escreva um programa que pergunte ao usuário qual o 
seu nível de conhecimento em TypeScript e exiba uma 
mensagem de acordo com o nível escolhido:
*/

namespace exercicio_1
{
    let nivel: string;
    nivel = "vançado";

    switch(nivel)
    {
        case "Iniciante": console.log("Seu nível é iniciante");
        break;
        case "Intermediário": console.log("Seu nível é intermediário");
        break;
        case "Avançado": console.log("Seu nível é avançado");
        break;
        default: console.log("Não é uma opção de resposta");
    }
}
