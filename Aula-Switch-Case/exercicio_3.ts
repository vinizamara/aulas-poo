//Sabor de sorvete
/*
Escreva um programa que pergunte ao usuário qual o seu 
sabor de sorvete favorito e exiba uma mensagem de acordo 
com o sabor escolhido:
*/

namespace exercicio_3
{
    let sabor: string;
    sabor = "Limão";

    switch(sabor)
    {
        case "Limão": console.log("Legal, limão também é meu sabor favorito!");
        break;
        case "Chocolate": console.log("Legal, chocolate é um sabor comum de sorvete!");
        break;
        case "Morango": console.log("Legal, morango é um sabor comum de sorvete!");
        break;
        case "Flocos": console.log("Legal, flocos é um sabor muito gostoso!");
        break;
        case "Açai": console.log("Legal, açai é um sabor bem único!");
        break;
        case "Coco": console.log("Legal, sabor tropical!");
        break;
        default: console.log("Legal, um sabor diferenciado!");
    }
}
