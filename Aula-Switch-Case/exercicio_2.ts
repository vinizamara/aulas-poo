//Cor favorita
/*
Escreva um programa que pergunte ao usuário qual a 
sua cor favorita e exiba uma mensagem de acordo com 
a cor escolhida:
*/

namespace exercicio_2
{
    let cor: string;
    cor = "Verde"

    switch(cor)
    {
        case "Verde": console.log("Legal, a cor do Palmeiras!");
        break;
        case "Amarelo": console.log("Legal, A cor amarela significa luz, calor, descontração, otimismo e alegria!");
        break;
        case "Azul": console.log("Legal, A cor azul significa tranquilidade, serenidade, harmonia e espiritualidade!");
        break;
        case "Roxo": console.log("Legal, A cor roxa está ligada ao mundo místico e significa espiritualidade, magia e mistério");
        break;
        case "Rosa": console.log("Legal, Rosa significa romantismo, ternura e ingenuidade!");
        break;
        case "Vermelho": console.log("Legal, A cor vermelha significa paixão, energia e excitação");
        break;
        case "Laranja": console.log("Legal, A cor laranja significa alegria, vitalidade, prosperidade e sucesso!");
        break;
        case "Marrom": console.log("Legal, A cor marrom significa conforto, segurança e simplicidade!");
        break;
        case "Branco": console.log("Legal, A cor branca significa paz, pureza e limpeza!");
        break;
        case "Preto": console.log("Legal, Você é do rock!💀");
        break;
        default: console.log("Legal, mas não temos nenhuma informação sobre essa cor!");
    }
}
