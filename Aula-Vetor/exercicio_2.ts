//Frutas
/*
Crie um array com 3 nomes de frutas. Em seguida,
use um loop while para iterar sobre o array e 
exibir cada fruta em uma linha separada.
*/

namespace exercicio_2
{
    let frutas: string[] = ["Banana", "Limão", "Laranja"];
    let i: number = 0;

    console.log("As frutas do menu são:");

    while(i < frutas.length)
    {
        console.log(frutas[i]);
        i++;
    }
}
