//Vetor alunos
/*
a)Crie um vetor chamado "alunos" contendo três
objetos, cada um representando um aluno com as
seguintes propriedades: "nome" (string),
"idade" (number) e "notas" (array de números).
Preencha o vetor com informações fictícias.

b)Em seguida, percorra o vetor utilizando a
função "forEach" e para cada aluno, calcule a
média das notas e imprima o resultado na tela,
juntamente com o nome e a idade do aluno.
*/

interface aluno
    {
        nome: string;
        idade: number;
        notas: number[];
    }

namespace exercicio_6
{
    const alunos: aluno[] = [
    {nome: "Aluno 1", idade: 20, notas:[3,3,3]},
    {nome: "Aluno 2", idade: 23, notas:[8,7,9]},
    {nome: "Aluno 3", idade: 243, notas:[5,6,10]},
    {nome: "Aluno 4", idade: 44, notas:[10,10,10]},
    {nome: "Aluno 5", idade: 12, notas:[10,7,4]},
    ];

    /*alunos.forEach((aluno) => {
        let media: number;
        media = (aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / 3;
        console.log("-------------");
        console.log(`A média aritmética do aluno é: ${media}`);
    })*/

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce((total, nota) => {return total + nota}) / aluno.notas.length;

        if (media >= 6)
        {
            console.log(`A média do aluno: ${aluno.nome} é igual a ${media} e está aprovado!!!`);
        }

        else
        {
            console.log(`A média do aluno: ${aluno.nome} é igual a ${media} e está reprovado!!!`);
        }
    })
}
