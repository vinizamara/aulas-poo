//Números Ímpares
/*
Crie um array com 6 números. Em seguida, use o método
filter() para criar um novo array contendo apenas os
números ímpares.
*/

namespace exercicio_4
{
    let numeros: number[] = [1, 2, 3, 4, 5, 6];
    let numerosImpar = numeros.filter(function(num){
        return num % 2 !== 0;
    })

    console.log(`Os números ímpares dessa sequência são: ${numerosImpar}`);
}
