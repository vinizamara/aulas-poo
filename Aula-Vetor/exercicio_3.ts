//
/*
Crie um array com 4 objetos, cada um representando um
livro com as propriedades titulo e autor.Em seguida, 
use o método map() para criar um novo array contendo
apenas os títulos dos livros.
*/

namespace exercicio_3
{
    let livros: any[] = [
        {titulo: "Diario de um banana", autor: "Jeff Kinney"}, 
        {titulo: "O pequeno principe", autor: "Antoine de Saint-Exupéry"}, 
        {titulo: "1984", autor: "George Orwell"}, 
        {titulo: "O Chamado de Cthulhu", autor: "H. P. Lovecraft"},
    ];

    let titulos = livros.map((livro) => {
        return livro.titulo;
    });

    let autores = livros.map((livro) => {
        return livro.autor;
    });

    /*let titulos = livros.map(function(item){
        return item.titulo;
    })
    let autores = livros.map(function(item){
        return item.autor;
    })*/

    console.log(`\nOs titulos dos livros são: ${titulos}\n\nE seus respectivos autores são: ${autores}\n`);
}
