//Autor 3

namespace atividade_sala{
let livros: any[] = [
    {titulo: "Três Pratos de Trigo", autor: "Autor 3"},
    {titulo: "Para Três Tigres Tristes", autor: "Autor 3"},
    {titulo: "O Juizo Final", autor: "Autor 3"},
    {titulo: "O Dia Que Eu Tropecei na Banana", autor: "Autor 3"},
    {titulo: "O Dia Que Eu Tropecei na Banana 2 (O Inimigo Agora é Outro)", autor: "Autor 3"},
];

let autor3 = livros.filter(function(autor){
    return autor.autor === "Autor 3"
});

let titulos = autor3.map(function(titulo){
    return titulo.titulo
});

console.log(`\nOs livros escritos pelo "Autor 3" são: ${titulos}\n`);
}
