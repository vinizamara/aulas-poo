//
/*
Crie um array vazio. Em seguida, use o método push() para
adicionar 3 números ao array.Em seguida, use o método pop()
para remover o último número do array e exibir o array
resultante.
*/

namespace exercicio_5
{
    let vetor: number[] = [];
    console.log(`O vetor vazio: [${vetor}]`);

    vetor.push(1, 3, 4);
    console.log(`O vetor com três números: [${vetor}]`);

    vetor.pop();
    console.log(`O vetor sem o último número: [${vetor}]`);

    /*let numeros: number[] = [];
    let i: number = 0;
    while(i < 1000)
    {
        numeros.push(i + 1);
        i++
    }
    console.log(numeros);*/
}
