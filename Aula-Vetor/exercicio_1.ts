//Soma de 5 números
/*
Crie um array com 5 números.Em seguida,
use um loop for para iterar sobre o array
e exibir a soma de todos os números.
*/

namespace exercicio_1
{
    let numeros: number[] = [1, 2, 3, 4, 5];
    let somaNumeros: number = 0;

    for(let i = 0; i < numeros.length; i++)
    {
        //somaNumeros = somaNumeros + numeros[i]
        somaNumeros += numeros[i];
    }

    console.log(`A soma dos cinco números é: ${somaNumeros}`);

    //Criando uma iteração com multiplicação
    let multiNumeros: number = 1;
    for(let i = 0; i < numeros.length; i++)
    {
        multiNumeros *= numeros[i];
    }

    console.log(`A multiplicação dos cinco números é: ${multiNumeros}`);
    
}
