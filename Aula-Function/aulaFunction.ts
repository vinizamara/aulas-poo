namespace aulaFunction{
    /*
    let numero1: number = 10;
    let numero2: number = 5;
    let soma: number = numero1 + numero2;
    */
    
    function soma(numero1:number, numero2:number){
        return numero1 + numero2;
    }

    console.log(soma(10, 5));
    console.log(soma(20, 15));
    let resultado: number;
    resultado = soma(40, 35);
    console.log(resultado * 2);

    function sub(numero1:number, numero2:number){
        return numero1 - numero2;
    }

    console.log('O resultado da subtração é:', sub(10, 5));

    function multi(numero1:number, numero2:number){
        return numero1 * numero2;
    }

    console.log('O resultado da multiplicação é', multi(10, 4));

    function div(numero1:number, numero2:number){
        return numero1 / numero2;
    }

    console.log('O resultado da divisão é:', div(24, 12));
}
