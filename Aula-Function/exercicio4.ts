//
/*
Crie uma função que receba um número como parâmetro
e retorne "par" se o número for par e "ímpar" se o 
número for ímpar.
*/

namespace exercicio4 {
    function parImpar(numero:number){
        if (numero % 2 == 0){
            return 'Par';
        }

        else{
            return 'Ímpar';
        }
    }

    console.log(parImpar(45));
}
